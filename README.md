
# User Service Assignment


## Solution Design

### Technology Used

Technologies used are

#### Maven
Maven was used to manage dependencies

#### Spring Boot
Spring boot was chosen to speed up development by providing automatic  Spring configurations and simple Maven Configurations. Spring boot also allows us to create stand-alone spring application with embedded Tomcat, therefore simplifying installation and enviroment dependencies. 


#### Spring WebFlux


#### MongoDab

## Simple Design
Controller > Service > DAO



## Installing

Simply build project and find jar executable in /target. Just run jar

java -jar UserService-0.0.1-SNAPSHOT.jar



## REST API 

### Authentication - Basic


### Get All Users

curl http://localhost:8080/user/ -v -u admin:password


### Create User

curl http://localhost:8080/user/ -X POST -d '{"firstName":"John","lastName":"Doe","mobileNumber":"08052678744","address":"Lagos"}' -H "Content-Type: application/json" -v -u admin:password

