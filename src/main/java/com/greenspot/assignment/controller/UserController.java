/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.assignment.controller;

import com.greenspot.assignment.model.User;
import com.greenspot.assignment.service.UserService;
import com.greenspot.assignment.util.ApiConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author mayowa.olurin
 */
@RestController
@RequestMapping(ApiConstants.USER_CONTEXT)
public class UserController {

    @Autowired
    UserService service;

    @RequestMapping(value = ApiConstants.BASE_PATH, method = RequestMethod.GET)
    Flux<User> findAll() {
        return service.getUsers();
    }

    @RequestMapping(value = ApiConstants.BASE_PATH, method = RequestMethod.POST)
    Mono<User> save(@RequestBody User user) {
        //Validate request

        return service.createUser(user)
                .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.BAD_REQUEST)));
    }

}
