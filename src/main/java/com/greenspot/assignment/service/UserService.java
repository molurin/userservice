/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.assignment.service;

import com.greenspot.assignment.model.User;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 *
 * @author mayowa.olurin
 */
@Service
public interface UserService {
    
    Flux<User> getUsers();
    
    Mono<User> createUser(User user);
    
}
