package com.greenspot.assignment.service.impl;

import com.greenspot.assignment.repository.UserRepository;
import com.greenspot.assignment.model.User;
import com.greenspot.assignment.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mayowa.olurin
 */
@Service
@Primary
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public Flux<User> getUsers() {
        return repository.findAll();
    }

    @Override
    public Mono<User> createUser(User user) {
        return repository.save(user);
    }

}
