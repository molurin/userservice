/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.greenspot.assignment.service;

import com.greenspot.assignment.model.User;
import com.greenspot.assignment.repository.UserRepository;
import com.greenspot.assignment.service.impl.UserServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import reactor.core.publisher.Mono;

/**
 *
 * @author mayowa.olurin
 */
public class UserServiceTest {

    private static final String FIRST_NAME = "Mayowa";
    private static final String LAST_NAME = "Olurin";
    private static final String MOBILE_NUMBER = "0800000002";
    private static final String ADDRESS = "Lagos";

    private UserService userService;
    private UserRepository userRepositoryMock;

    @Before
    public void setUp() {
        userRepositoryMock = mock(UserRepository.class);
        userService = new UserServiceImpl(userRepositoryMock);
    }

    @Test
    public void createUserTest() {
        try {

            User sampleUser = getSampleUserModel();
            Mono userMono = Mono.just(sampleUser);
            when(userRepositoryMock.save(any(User.class))).thenReturn(userMono);

            Mono<User> returned = userService.createUser(sampleUser);

            assertNotNull(returned);

        } catch (Exception ex) {
            Logger.getLogger(UserServiceTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private User getSampleUserModel() {
        User savedUser = new User();
        savedUser.setFirstName(FIRST_NAME);
        savedUser.setLastName(LAST_NAME);
        savedUser.setMobileNumber(MOBILE_NUMBER);
        savedUser.setAddress(ADDRESS);

        return savedUser;
    }

}
